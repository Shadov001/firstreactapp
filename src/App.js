import React from 'react';
import './App.css';
import Header from './components/Header.js';
import MoviesContainer from './app/movies/components/MoviesContainer'
import todosData from './todosData'
import ToDoItem from './components/ToDoItem'

class App extends React.Component {
  
  constructor() {
    super()
    this.state = {
        info: "Initial state",
        isLoggedIn: true,
        todos: todosData,
        counter: 0
    }
    this.handleChange = this.handleChange.bind(this)
    this.addToCounter = this.addToCounter.bind(this)
  }

  handleChange(id){
    const list = this.state.todos.slice();
    const index = list.findIndex(o => o.id === id);
    list[index].completed = !list[index].completed;
    this.setState({ todos: list });
}

  addToCounter(){
      this.setState(prevState => {
        return {
           counter: prevState.counter + 1
        }
      })
  }

  render()  {
    const todoItems = this.state.todos.map(item => <ToDoItem key={item.id} todoItem={item} 
    handleChange={this.handleChange}/>) 

    return (
    <div className="App">
      <Header initialState={this.state.info}/>
      {/* <MoviesContainer/>  */}
      {todoItems}
      <h1>{this.state.counter}</h1>
      <button onClick={this.addToCounter}>Count!</button>
      <h1>You are currently logged {this.state.isLoggedIn ? "in" : "out"}</h1>
    </div>
    );
  }

}

export default App;
