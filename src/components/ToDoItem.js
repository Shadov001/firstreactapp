import React from 'react'

function ToDoItem(props){
    return (
        <div className = "todo-list">
            <div className = "todo-item">
                <input type="checkbox" 
                    checked={props.todoItem.completed} 
                    onChange = {() => props.handleChange(props.todoItem.id)}/>
                <p>{props.todoItem.text}</p>
            </div>
        </div>
    )
}

export default ToDoItem