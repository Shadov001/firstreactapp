import React from 'react';

class Header extends React.Component{
    render(){
        const headerIntroduction = "I'm Marcin";
        const headerTemplate = "this is my header!";
        const date = new Date();
        const hours = date.getHours();
    
        const styles = {
            color: "red",
            backgroundColor: "yellow"
        }
    
        let timeOfDay;
        if(hours > 12){
            timeOfDay = "afternoon"
            styles.color = "blue"
        }
        else {
            timeOfDay = "morning"
            styles.color = "purple"
        }

    return (
    <header className="myHeader">
        {`${headerIntroduction} and ${headerTemplate}`} <br></br>
        It's currently about {date.getHours() % 12} o'clock! <br></br>
        Good {timeOfDay}! 
        {/* <h4 style={styles}>Enjoy!</h4> */} <br></br>
        Welcome, the state is: {this.props.initialState}!
    </header>
    )
    }
  }

  export default Header;