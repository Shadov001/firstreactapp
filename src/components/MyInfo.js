import React from 'react';

class MyInfo extends React.Component{
  render(){
    return (
      <div>
      <h1>
        Marcin Jamróz
      </h1>
      <p>
        This is info about myself
      </p>
      <ul>
         <li>first</li>
         <li>second</li>
         <li>third</li>
      </ul>
      <ol>
         <li>first</li>
         <li>second</li>
         <li>third</li>
      </ol>
      </div>
    )
  }
}


  export default MyInfo;