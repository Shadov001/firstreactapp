import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import  rootReducer  from './reducers';

//kreator funkcji
//store.dispatch({type:'ADD_ACTOR', actor: 'Marcin Jamróz'});
// const addActor = actor => ({type: 'ADD_ACTOR', actor});
// const resetActors = () => ({type: 'RESET_ACTORS'});
// store.dispatch(addActor('Nowy Aktor'));

//binder
// const actorsActions = bindActionCreators({add: addActor, resetActors}, store.dispatch);
// actorsActions.add('Aktor Nowy');
// actorsActions.resetActors();

const store = createStore(rootReducer, composeWithDevTools()) 
//umożliwia debug w przegladarce

export default store;