const todosData = [
    {
        id: 1,
        text: "Do the washing",
        completed: true
    },
    {
        id: 2,
        text: "Take out the trash",
        completed: false
    },
    {
        id: 3,
        text: "Do the shopping",
        completed : false
    },
    {
        id: 4,
        text: "Play some games",
        completed: true
    },
    {
        id: 5,
        text: "Mow lawn",
        completed: false
    },
    {
        id: 6,
        text: "Write an essay",
        completed: true
    }
]

export default todosData