import types from './types'

const add = actor => ({type: types.ADD_ACTOR, actor});
const reset = () => ({type: types.RESET_ACTORS});
const change_listname = actorslistName => ({type: types.CHANGE_ACTORS_LISTNAME, actorslistName});

export default {
    add, 
    reset,
    change_listname
}