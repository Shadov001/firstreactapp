import types from './types'

const INITIAL_STATE = {
    actorslistName: 'Actors',
    actors: [
      'Tom Hanks', 'Julia Roberts', 'Angelina Jolie'
    ]
  }

const actorsReducer = (state = INITIAL_STATE, action) => {  //reducer
    switch (action.type) {
      case types.CHANGE_ACTORS_LISTNAME:
        return {
          ...state, actorslistName: action.actorslistName  //zmieniamy nazwę listy filmów
        }
      case types.ADD_ACTOR:
        return {
          ...state, actors: [...state.actors, action.actor]  //operacja o typie ADD zachowa stan całego obiektu, zachowa stan tablicy movies oraz doda kolejny element do tablicy movies poprzez parametr movie
        }
      case types.RESET_ACTORS:
        return {
          ...state, actors: []  //za każdym razem tworzymy nową wersje state, a zmieniamy tylko niektóre pola
        }
      default:
        return state
    }
  }

  export default {
      actorsReducer
  }