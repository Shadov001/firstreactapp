import types from './types';

const INITIAL_STATE = {
    movieslistName: 'Movies',
    movies: [
      'Film1', 'Film2', 'Film3'
    ]
  }

const moviesReducer = (state = INITIAL_STATE, action) => {  //reducer
    switch (action.type) {
      case types.CHANGE_MOVIES_LISTNAME:
        return {
          ...state, movieslistName: action.movieslistName  //zmieniamy nazwę listy filmów
        }
      case types.ADD_MOVIE:
        return {
          ...state, movies: [...state.movies, action.movie]  //operacja o typie ADD zachowa stan całego obiektu, zachowa stan tablicy movies oraz doda kolejny element do tablicy movies poprzez parametr movie
        }
      case types.RESET_MOVIES:
        return {
          ...state, movies: []  //za każdym razem tworzymy nową wersje state, a zmieniamy tylko niektóre pola
        }
      default:
        return state
    }
  }

export default {
      moviesReducer
};
