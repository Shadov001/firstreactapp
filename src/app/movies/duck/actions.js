import types from './types'

const add = movie => ({type: types.ADD_MOVIE, movie});
const reset = () => ({type: types.RESET_MOVIES});
const change_listname = movieslistName => ({type: types.CHANGE_MOVIES_LISTNAME, movieslistName});

export default {
    add, 
    reset,
    change_listname
}